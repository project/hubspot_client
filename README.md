# Hubspot Client

This module provides a client for the Hubspot API v3 SDK. It is intended
to be used by other modules to integrate with Hubspot.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Requirements

The module requires the [Hubspot Api](https://www.drupal.org/project/hubspot_api) 
module. Unfortunately, the Hubspot Api module branch 2.x and release 2.0.2 is not
compatible with the Hubspot Api v3 SDK. Needs to add the follow repository to 
composer.json file **repositories** section:
```json
{
  "type": "package",
  "package": {
    "name": "drupal/hubspot_api",
    "type": "drupal-module",
    "version": "dev-8.x-2.x",
    "source": {
      "type": "git",
      "url": "https://git.drupalcode.org/project/hubspot_api",
      "reference": "70251ba04f82a6908b5e20a7f619250b4d86b081"
    }
  }
}
```
In **require** section use above repository version: "drupal/hubspot_api": "dev-8.x-2.x"

And use this patch:
https://www.drupal.org/files/issues/2023-05-02/hubspot_api-switch-to-hubspot-api-v3-3334746-7.patch

Add the new hubspot/api-client:
```
composer require hubspot/api-client:^9.4 -W
composer install -o
```

## Configuration

The module does not provide any configuration.

## Usage

In case you do not need special fields sync to Hubspot, you need just
enable the module and set the Hubspot Private app key in the
Hubspot api settings form.

The base module sync user (contacts) to Hubspot.

Provides two other modules to sync content.
- The hubspot_commerce module sync commerce_orders, commerce_order_items
and commerce_products to Hubspot.
- The hubspot_sync module sync Hubspot objects to Drupal entities to set
Hubspot object id to Drupal entity hubspot_id field. After this process you
can sync the Drupal entities to Hubspot objects.

In case you need special entities or/and fields sync to Hubspot, you need to
create a custom module and implement event subscriber to the following events:
- EntityMappingEvent
- FieldMappingSyncFromEvent
- FieldMappingSyncToEvent
- FieldSyncIdentifyFieldsEvent
- SyncFromEntityUpdateEvent
- SyncPostRequestEvent
- SyncPreRequestEvent

Find example in the hubspot_commerce module.

In case you need other Drupal entities sync to Hubspot, you need to implement
the hook_entity_base_field_info hook and add the hubspot_id field to the
entity.
```php
/**
 * Implements hook_entity_base_field_info().
 */
function mymodule_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  $entity_types = [
    'my_entity_type_id',
    'my_other_entity_type_id',
  ];
  foreach ($entity_types as $entity_type_name) {
    if ($entity_type->id() === $entity_type_name) {
      $fields['hubspot_id'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Hubspot ID'))
        ->setDescription(t('The Hubspot ID for entity.'))
        ->setSettings([
          'max_length' => 255,
          'default_value' => NULL,
        ]);
    }
  }

  return $fields;
}
```
If added this field to existing module you need to implement
the update hook as well.

## Supporting organizations


## Maintainers

Current maintainer:

- [Dudás József](https://www.drupal.org/u/dj1999) - [Drupal.org]

