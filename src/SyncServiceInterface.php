<?php

namespace Drupal\hubspot_client;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for the SyncFromService class.
 *
 * This service is responsible for syncing Hubspot entities with
 * Drupal entities.
 * Ie. syncing Hubspot contacts with Drupal users.
 *
 * This service is also responsible for syncing Drupal entities with Hubspot.
 * Ie. syncing Drupal users with Hubspot contacts.
 *
 * @package Drupal\hubspot_api\Hubspot
 */
interface SyncServiceInterface {

  /**
   * Syncs a Hubspot entity with the appropriate Drupal entity.
   *
   * @param array<mixed> $hubspot_entity
   *   The Hubspot entity to sync.
   */
  public function syncFrom(array $hubspot_entity): void;

  /**
   * Sync an entity with Hubspot.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we're syncing (ie. user/order/product variation).
   * @param string $op
   *   The operation we're performing (ie. create/update/archive).
   * @param \Drupal\Core\Entity\EntityInterface|null $parent
   *   The parent entity (ie. order for order items).
   * @param bool $debug
   *   Flag for debugging.
   *
   * @return bool
   *   True if sync is success otherwise false.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function syncTo(EntityInterface $entity, string $op, EntityInterface $parent = NULL, bool $debug = FALSE): bool;

}
