<?php

namespace Drupal\hubspot_client;

use Drupal\hubspot_client\Event\EntityMappingEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncToEvent;
use Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent;
use Drupal\hubspot_client\Event\SyncPostRequestEvent;
use Drupal\hubspot_client\Event\SyncPreRequestEvent;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hubspot_api\ManagerInterface;
use Drupal\hubspot_client\Event\SyncFromEntityUpdateEvent;
use HubSpot\Client\Crm\Objects\ApiException;
use HubSpot\Client\Crm\Objects\Model\Error;
use HubSpot\Client\Crm\Objects\Model\Filter;
use HubSpot\Client\Crm\Objects\Model\FilterGroup;
use HubSpot\Client\Crm\Objects\Model\ModelInterface;
use HubSpot\Client\Crm\Objects\Model\PublicObjectSearchRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The SyncFromService class.
 *
 * Contains functions for synchronizing data from the HubSpot API to Drupal.
 *
 * @package Drupal\commerce_hubspot
 */
class SyncService implements SyncServiceInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The client.
   *
   * @var \HubSpot\Discovery\Discovery
   */
  protected $client;

  /**
   * An event dispatcher instance.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The TimeInterface definition.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;

  /**
   * The store id to sync data.
   *
   * @var string
   */
  protected $store;

  /**
   * The HubSpot id to sync data.
   *
   * @var string
   */
  protected $hubspotId;

  /**
   * The entity to sync data.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Constructs a new HubSpot Commerce service instance.
   *
   * @param \Drupal\hubspot_api\ManagerInterface $hubspot_manager
   *   The Hubspot API Manager class.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manger.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_factory
   *   The logger channel.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   *
   * @throws \Exception
   */
  public function __construct(
    ManagerInterface $hubspot_manager,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelInterface $logger_factory,
    EventDispatcherInterface $event_dispatcher,
    TimeInterface $time
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory;
    $this->eventDispatcher = $event_dispatcher;
    $this->dateTime = $time;
    // Initialize our Hubspot API client.
    $this->client = $hubspot_manager->getHandler();
  }

  /**
   * {@inheritdoc}
   */
  public function syncFrom(array $hubspot_entity): void {
    try {
      // Dispatch an event to allow modules to update and save this entity.
      $event = new SyncFromEntityUpdateEvent($hubspot_entity);
      $this->eventDispatcher->dispatch($event);
    }
    catch (\Exception $e) {
      $error = 'An error occurred while syncing from Hubspot to Drupal. The hubspot entity type is: ';
      $error .= $hubspot_entity['entity_type'];
      $error .= ' and entity is: ' . json_encode($hubspot_entity['entity']);
      $error .= '. The error was: ' . $e->getMessage();

      $this->logger->error($error);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function syncTo(EntityInterface $entity, string $op, EntityInterface $parent = NULL, bool $debug = FALSE): bool {
    if ($op === 'update' && $this->justSetHubspotId($entity)) {
      // Do not sync again if we just set the Hubspot ID.
      return TRUE;
    }
    if ($entity->get('hubspot_id')->isEmpty() && $op === 'update') {
      // Set op to 'create' if we are updating an entity that has no Hubspot ID.
      $op = 'create';
    }
    // Dispatch an event to allow modules to tell us which Hubspot entity and ID
    // to sync this Drupal entity with.
    $event = new EntityMappingEvent([]);
    $this->eventDispatcher->dispatch($event);
    $entity_mapping = $event->getEntityMapping();

    // If entity type isn't mapped then return.
    if (!in_array($entity->getEntityTypeId(), array_keys($entity_mapping))) {
      return FALSE;
    }

    // If entity huspot id isn't empty and operation is 'create' then return.
    if (($entity->hasField('hubspot_id') &&
        !$entity->get('hubspot_id')->isEmpty() &&
        $op === 'create') ||
      !$entity->hasField('hubspot_id')) {
      return TRUE;
    }

    // Reset the entity cache as we might have an outdated entity.
    $this->entityTypeManager
      ->getStorage($entity->getEntityTypeId())->resetCache([$entity->id()]);

    // Now, dispatch another event to allow modules to define which Drupal
    // fields will be synced to which HubSpot fields for this entity.
    $event = new FieldMappingSyncToEvent($entity);
    $this->eventDispatcher->dispatch($event);
    $field_mapping = $event->getFieldMapping()[$entity->getEntityTypeId()] ?? [];
    // If field mapping is empty then return.
    if (empty($field_mapping)) {
      return FALSE;
    }
    $field_mapping['parent'] = $parent;

    $this->entity = $entity;
    $this->hubspotId = $entity->get('hubspot_id')->getString();

    try {
      // Now, do the actual syncing depending on the entity type.
      return $this->syncEntity($entity->getEntityTypeId(), $field_mapping, $op, $debug);
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Syncs the entity details with Hubspot.
   *
   * @param string $type
   *   The hubspot type.
   * @param array<mixed> $hubspot_payload
   *   An array of Hubspot sync message properties.
   * @param string $op
   *   The operation flag.
   * @param bool $debug
   *   Teh debugging flag.
   *
   * @return bool
   *   True if sync success. False otherwise.
   */
  protected function syncEntity(string $type, array $hubspot_payload, string $op, bool $debug): bool {
    if ($debug === TRUE) {
      $log_data = $hubspot_payload;
      unset($log_data['parent']);
      $this->logger->debug(
        'Data to sync a @type to Hubspot. The payload is: @payload.', [
          '@type' => $type,
          '@payload' => json_encode($log_data, JSON_THROW_ON_ERROR),
        ]
      );
    }
    $parent = $hubspot_payload['parent'] ?? NULL;
    // Dispatch an event to allow modules to modify payload.
    $event = new SyncPreRequestEvent($this->entity, $hubspot_payload);
    $this->eventDispatcher->dispatch($event);
    $data = $event->getData();
    /** @var \HubSpot\Client\Crm\Objects\Model\ModelInterface $payload_data */
    $payload_data = $data['payload'] ?? [];
    $huspot_object_type = $event->getObjectType();

    $response = $status = $header = NULL;
    try {
      switch ($op) {
        case 'create':
          // Create the Hubspot object.
          [$response, $status, $header] = $this->client->crm()->objects()->basicApi()
            ->createWithHttpInfo($huspot_object_type, $payload_data);
          break;

        case 'update':
          // Update the Hubspot object.
          [$response, $status, $header] = $this->client->crm()->objects()->basicApi()
            ->updateWithHttpInfo($huspot_object_type, $this->hubspotId, $payload_data);
          break;

        case 'archive':
          // Delete the Hubspot object.
          [$response, $status, $header] = $this->client->crm()->objects()->basicApi()
            ->archiveWithHttpInfo($huspot_object_type, $this->hubspotId);
          break;
      }

      if ($response instanceof ModelInterface) {
        // Dispatch an event to allow modules to react to the response.
        $event = new SyncPostRequestEvent($response, [
          'entity' => $this->entity,
          'op' => $op,
          'hubspot_id' => $this->hubspotId,
          'type' => $huspot_object_type,
          'data' => $payload_data,
          'parent' => $parent,
          'log' => $data['log'] ?? '',
          'status' => $status,
          'header' => $header,
        ]);
        $this->eventDispatcher->dispatch($event);
      }

      if ($response instanceof Error) {
        throw new ApiException($response->getMessage(), $status, $header);
      }
      if ($op === 'create') {
        // We were successful, write hubspot id to object.
        $this->entity->set('hubspot_id', $response->getId());
        $this->entity->save();
      }
      return TRUE;
    }
    catch (ApiException $e) {
      $response = $e->getResponseObject();
      if ($response instanceof ModelInterface) {
        // Dispatch an event to allow modules to react to the response.
        $event = new SyncPostRequestEvent($response, [
          'entity' => $this->entity,
          'op' => $op,
          'hubspot_id' => $this->hubspotId,
          'type' => $huspot_object_type,
          'data' => $payload_data,
          'parent' => $parent,
          'log' => $data['log'] ?? '',
          'exception' => $e,
          'status' => $e->getCode(),
          'header' => $e->getResponseHeaders(),
        ]);
        $this->eventDispatcher->dispatch($event);
      }

      if ($e->getCode() === 409) {
        // The entity already exists in Hubspot, so we need to update it.
        return $this->setExistingObject($type, $hubspot_payload, $huspot_object_type);
      }
      // Bad response.
      $this->logger->warning(
        'An error occurred while trying to sync a @type to Hubspot. The payload is: @payload. The error was: @error', [
          '@type' => $type,
          '@payload' => json_encode($hubspot_payload, JSON_THROW_ON_ERROR),
          '@error' => $e->getMessage(),
        ]
      );

      return FALSE;
    }
  }

  /**
   * Update an existing object in Hubspot.
   *
   * @param string $type
   *   The hubspot type.
   * @param array<mixed> $hubspot_payload
   *   An array of Hubspot sync payload.
   * @param string $huspot_object_type
   *   The hubspot object type.
   *
   * @return bool
   *   True if sync success. False otherwise.
   *
   * @throws \HubSpot\Client\Crm\Objects\ApiException
   */
  protected function setExistingObject(string $type, array $hubspot_payload, string $huspot_object_type): bool {
    $event = new FieldSyncIdentifyFieldsEvent();
    $this->eventDispatcher->dispatch($event);
    $field_mapping = $event->getFieldMapping()[$type] ?? [];
    $key = key($field_mapping);
    if (isset($hubspot_payload['properties'][$key])) {
      try {
        $filter = new Filter();
        $filter->setOperator('EQ')
          ->setPropertyName($key)
          ->setValue($hubspot_payload['properties'][$key]);

        $group = new FilterGroup();
        $group->setFilters([$filter]);

        $request = new PublicObjectSearchRequest(['filter_groups' => [$group]]);

        $response = $this->client->crm()->objects()->searchApi()
          ->doSearch($huspot_object_type, $request)->getResults()[0] ?? NULL;
        if ($response !== NULL) {
          $this->entity->set('hubspot_id', $response->getId());
          $this->entity->save();
          return TRUE;
        }
      }
      catch (ApiException $e) {
        $this->logger->warning(
          'An error occurred while trying to sync a @type to Hubspot. The payload is: @payload. The error was: @error', [
            '@type' => $type,
            '@payload' => json_encode($request->jsonSerialize(), JSON_THROW_ON_ERROR),
            '@error' => $e->getMessage(),
          ]
        );
      }
    }

    return FALSE;
  }

  /**
   * Check if entity is just added hubspot id.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   True if entity is just created hubspot id. False otherwise.
   */
  protected function justSetHubspotId(EntityInterface $entity): bool {
    if (isset($entity->original) && $entity->hasField('hubspot_id')) {
      if ($entity->original->get('hubspot_id')->isEmpty() &&
        !$entity->get('hubspot_id')->isEmpty()) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
