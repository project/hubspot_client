<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

use HubSpot\Client\Crm\Objects\Model\SimplePublicObjectInput;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Drupal fields will be synced to which HubSpot
 * fields.
 *
 * @package Drupal\hubspot_client\Event
 */
class FieldMappingSyncToEvent extends Event {

  /**
   * The entity that is about to be synced.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * An array with the Hubspot field names and their Drupal sync values.
   *
   * The array key is the Drupal entity type.
   *
   * @var array<mixed>
   *   IE. return [
   *     'entity_type_id' => [
   *       'properties' => [
   *         'hs_property1' => 'value1',
   *         'hs_property2' => 'value2',
   *        ],
   *     'associations' => [
   *       [
   *         'to' => [
   *           'id' => $hs_order_id,
   *         ],
   *         'types' => [
   *           [
   *             'associationCategory' => 'HUBSPOT_DEFINED',
   *             'associationTypeId' => '20',
   *           ],
   *         ],
   *       ],
   *     ],
   *   ];
   */
  protected $fieldMapping;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that is about to be synced.
   * @param array<mixed> $field_mapping
   *   An array of Drupal field names with the Hubspot mapping information.
   */
  public function __construct(EntityInterface $entity = NULL, array $field_mapping = []) {
    $this->entity = $entity;
    if ($entity->getEntityTypeId() === 'user') {
      $this->setUserFieldMapping($field_mapping);
    }

    $this->setFieldMapping($field_mapping);
  }

  /**
   * Prepare user field map for sync.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping array.
   */
  protected function setUserFieldMapping(array &$field_mapping): void {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entity;
    $field_mapping['user']['properties'] = [
      'email' => $user->getEmail() ?? '',
      'lifecyclestage' => 'salesqualifiedlead',
      'hs_language' => $user->getPreferredLangcode(),
    ];
  }

  /**
   * Gets the field mapping array.
   *
   * @return array<mixed>
   *   The field mapping array.
   */
  public function getFieldMapping(): array {
    return $this->fieldMapping;
  }

  /**
   * Sets the field mapping.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping array.
   *
   * @return \Drupal\hubspot_client\Event\FieldMappingSyncToEvent
   *   This object.
   */
  public function setFieldMapping(array $field_mapping): self {
    $this->fieldMapping = $field_mapping;
    return $this;
  }

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Sets the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\hubspot_client\Event\FieldMappingSyncToEvent
   *   This object.
   */
  public function setEntity(EntityInterface $entity): self {
    $this->entity = $entity;
    return $this;
  }

  /**
   * Gets the hubspot input data.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return \HubSpot\Client\Crm\Objects\Model\SimplePublicObjectInput
   *   The hubspot input data.
   */
  public function getHubspotInputData(string $entity_type): SimplePublicObjectInput {
    $mapping = $this->fieldMapping[$entity_type]['properties'] ?? [];
    return new SimplePublicObjectInput($mapping);
  }

}
