<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Component\EventDispatcher\Event;
use HubSpot\Client\Crm\Objects\Model\ModelInterface;
use HubSpot\Crm\ObjectType;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Drupal fields will be synced to which HubSpot
 * fields.
 *
 * @package Drupal\hubspot_client\Event
 */
class FieldMappingSyncFromEvent extends Event {

  /**
   * The object that is being synced from hubspot.
   *
   * @var \HubSpot\Client\Crm\Objects\Model\ModelInterface
   */
  protected $object;

  /**
   * An array with the Drupal and Hubspot fields and their sync values.
   *
   * The array key is the Drupal field name while the id is the hubspot field
   * name.
   *
   * @var array<mixed>
   *   IE. return [
   *     'entity_type_id' => [
   *       'id' => [
   *          'hubspot_id' => 'id',
   *          'status' => TRUE,
   *          'value' => 'value',
   *       ],
   *       'email' => [
   *          'id' => 'mail',
   *          'status' => TRUE,
   *          'value' => 'value',
   *       ],
   *    ];
   */
  protected $fieldMapping;

  /**
   * Constructs the object.
   *
   * @param \HubSpot\Client\Crm\Objects\Model\SimplePublicObjectWithAssociations $object
   *   The object that is being synced from hubspot.
   * @param array<mixed> $field_mapping
   *   An array of Drupal field names with the Hubspot mapping information.
   */
  public function __construct(ModelInterface $object = NULL, array $field_mapping = []) {
    $this->object = $object;
    $properties = $object !== NULL ? $object->getProperties() : [];
    $fields = [
      ObjectType::CONTACTS => [
        'id' => [
          'id' => 'hubspot_id',
          'status' => TRUE,
          'value' => $object !== NULL ? $object->getId() : '',
        ],
        'email' => [
          'id' => 'mail',
          'status' => TRUE,
          'value' => $properties['email'] ?? '',
        ],
        'hs_language' => [
          'id' => 'prefered_language',
          'status' => TRUE,
          'value' => $properties['hs_language'] ?? '',
        ],
        'hs_analytics_last_timestamp' => [
          'id' => 'last_access',
          'status' => TRUE,
          'value' => $properties['hs_analytics_last_timestamp'] ?? '',
        ],
      ],
    ];

    $field_mapping = array_merge($field_mapping, $fields);
    $this->fieldMapping = $field_mapping;
  }

  /**
   * Gets the field mapping array.
   *
   * @return array<mixed>
   *   The field mapping array.
   */
  public function getFieldMapping(): array {
    return $this->fieldMapping;
  }

  /**
   * Sets the field mapping.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping array.
   *
   * @return \Drupal\hubspot_client\Event\FieldMappingSyncFromEvent
   *   This object.
   */
  public function setFieldMapping(array $field_mapping): self {
    $this->fieldMapping = $field_mapping;
    return $this;
  }

  /**
   * Gets the object.
   *
   * @return \HubSpot\Client\Crm\Objects\Model\ModelInterface|null
   *   The object or null.
   */
  public function getObject():? ModelInterface {
    return $this->object;
  }

}
