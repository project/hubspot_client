<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that gets dispatched when an entity is synced from Hubspot.
 *
 * Allows modules to interact during the entity update process.
 *
 * @package Drupal\hubspot_client\Event
 */
class SyncFromEntityUpdateEvent extends Event {

  /**
   * The Hubspot entity that was updated.
   *
   * @var \HubSpot\Client\Crm\Objects\Model\ModelInterface
   */
  protected $hubspotEntity;

  /**
   * The Hubspot entity type that was updated.
   *
   * @var string
   */
  protected $hubspotEntityType;

  /**
   * Constructs the SyncFromEntityUpdateEvent object.
   *
   * @param array<mixed> $hubspot_entity
   *   The Hubspot entity that was updated.
   */
  public function __construct(array $hubspot_entity) {
    $this->hubspotEntityType = $hubspot_entity['entity_type'];
    $this->hubspotEntity = $hubspot_entity['entity'];
  }

  /**
   * Gets the Hubspot entity type.
   *
   * @return string
   *   The Hubspot entity type.
   */
  public function getHubspotEntityType(): string {
    return $this->hubspotEntityType;
  }

  /**
   * Gets the Hubspot entity.
   *
   * @return \HubSpot\Client\Crm\Objects\Model\ModelInterface
   *   The Hubspot entity.
   */
  public function getHubspotEntity() {
    return $this->hubspotEntity;
  }

}
