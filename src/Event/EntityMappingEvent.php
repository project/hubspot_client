<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Component\EventDispatcher\Event;
use HubSpot\Crm\ObjectType;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Hubspot entity and ID a Drupal entity
 * will be mapped to.
 *
 * @package Drupal\hubspot_client\Event
 */
class EntityMappingEvent extends Event {

  /**
   * An array defining which Hubspot entity this entity should be mapped to.
   *
   * @var array<mixed>
   *   IE. return [
   *     'user' => 'contacts',
   *   ];
   */
  protected $entityMapping;

  /**
   * Constructs the object.
   *
   * @param array<mixed> $entity_mapping
   *   The Hubspot entity and ID that this Drupal entity should be mapped to.
   */
  public function __construct(array $entity_mapping) {
    $entity_mapping = array_merge($entity_mapping, [
      'user' => ObjectType::CONTACTS,
    ]);
    $this->entityMapping = $entity_mapping;
  }

  /**
   * Gets the entity mapping array.
   *
   * @return array<mixed>
   *   The entity mapping array.
   */
  public function getEntityMapping(): array {
    return $this->entityMapping;
  }

  /**
   * Sets the entity mapping.
   *
   * @param array<mixed> $entity_mapping
   *   The entity mapping array.
   *
   * @return \Drupal\hubspot_client\Event\EntityMappingEvent
   *   This object.
   */
  public function setEntityMapping(array $entity_mapping): self {
    $this->entityMapping = $entity_mapping;
    return $this;
  }

}
