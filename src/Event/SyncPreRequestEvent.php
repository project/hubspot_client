<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Core\Entity\EntityInterface;

use Drupal\Component\EventDispatcher\Event;
use HubSpot\Client\Crm\Objects\Model\SimplePublicObjectWithAssociations;
use HubSpot\Crm\ObjectType;

/**
 * Event that gets dispatched when an entity is updated.
 *
 * Allows modules to define which Drupal entities should be synced to Hubspot.
 *
 * @package Drupal\hubspot_client\Event
 */
class SyncPreRequestEvent extends Event {

  /**
   * The entity that's being updated.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The array of entity types to sync.
   *
   * @var array<mixed>
   */
  protected $hubspotPayload;

  /**
   * The user defined data definition.
   *
   * @var array<mixed>
   */
  protected $data = [];

  /**
   * The object type.
   *
   * @var string
   */
  protected $objectType;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that's being updated.
   * @param array<mixed> $payload
   *   The array of payload to sync.
   */
  public function __construct(EntityInterface $entity, array $payload) {
    $this->entity = $entity;
    $this->hubspotPayload = $payload;
    if ($entity->getEntityTypeId() === 'user') {
      $input = new SimplePublicObjectWithAssociations();
      $input->setProperties(($payload['properties'] ?? []));
      $input->setAssociations(($payload['associations'] ?? []));
      $this->data['payload'] = $input;
      $this->objectType = ObjectType::CONTACTS;
    }
  }

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Gets the entities to sync.
   *
   * @return array<mixed>
   *   The entities to sync.
   */
  public function getPayload(): array {
    return $this->hubspotPayload;
  }

  /**
   * Sets the entities to sync.
   *
   * @param array<mixed> $payload
   *   The entities to sync.
   *
   * @return \Drupal\hubspot_client\Event\SyncPreRequestEvent
   *   The self object.
   */
  public function setPayload(array $payload): self {
    $this->hubspotPayload = $payload;
    return $this;
  }

  /**
   * Return user data to using on post request event.
   *
   * @return array<mixed>
   *   The user defined data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Set user data to using on post request event.
   *
   * @param array<mixed> $data
   *   The user defined data.
   *
   * @return \Drupal\hubspot_client\Event\SyncPreRequestEvent
   *   The self object.
   */
  public function setData(array $data): self {
    $this->data = $data;
    return $this;
  }

  /**
   * Return object type to using on post request event.
   *
   * @return string
   *   The object type.
   */
  public function getObjectType() {
    return $this->objectType;
  }

  /**
   * Set object type to using on post request event.
   *
   * @param string $type
   *   The object type.
   *
   * @return $this
   */
  public function setObjectType(string $type) {
    $this->objectType = $type;
    return $this;
  }

}
