<?php

namespace Drupal\hubspot_client\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Drupal fields will be synced to which HubSpot
 * fields.
 *
 * @package Drupal\hubspot_client\Event
 */
class FieldSyncIdentifyFieldsEvent extends Event {

  /**
   * An array with the Drupal and Hubspot fields and their sync values.
   *
   * The array key is the Drupal field name while the id is the hubspot field
   * name.
   *
   * @var array<mixed>
   *   IE. return [
   *     'entity_type_id' => ['email' => 'mail'],
   *     'entity_type_id2' => ['hs_sku' => 'sku'],
   *    ];
   */
  protected $fieldMapping;

  /**
   * Constructs the object.
   *
   * @param array<mixed> $field_mapping
   *   An array of Drupal field names with the Hubspot mapping information.
   */
  public function __construct(array $field_mapping = []) {
    $fields = [
      'user' => ['email' => 'mail'],
    ];
    $field_mapping = array_merge($field_mapping, $fields);
    $this->fieldMapping = $field_mapping;
  }

  /**
   * Gets the field mapping array.
   *
   * @return array<mixed>
   *   The field mapping array.
   */
  public function getFieldMapping(): array {
    return $this->fieldMapping;
  }

  /**
   * Sets the field mapping.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping array.
   *
   * @return \Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent
   *   This object.
   */
  public function setFieldMapping(array $field_mapping): self {
    $this->fieldMapping = $field_mapping;
    return $this;
  }

}
