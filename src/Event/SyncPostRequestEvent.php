<?php

namespace Drupal\hubspot_client\Event;

use HubSpot\Client\Crm\Objects\Model\ModelInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that gets dispatched when an entity is updated.
 *
 * Allows modules to define which Drupal entities should be synced to Hubspot.
 *
 * @package Drupal\hubspot_client\Event
 */
class SyncPostRequestEvent extends Event {

  /**
   * The sync response.
   *
   * @var \HubSpot\Client\Crm\Objects\Model\ModelInterface
   */
  protected $response;

  /**
   * The user defined array.
   *
   * @var array<mixed>
   */
  protected $data;

  /**
   * Constructs the object.
   *
   * @param \HubSpot\Client\Crm\Objects\Model\ModelInterface $response
   *   The sync response.
   * @param array<mixed> $data
   *   The user defined data.
   */
  public function __construct(ModelInterface $response, array $data) {
    $this->response = $response;
    $this->data = $data;
  }

  /**
   * Gets the response.
   *
   * @return \HubSpot\Client\Crm\Objects\Model\ModelInterface
   *   The entity.
   */
  public function getResponse(): ModelInterface {
    return $this->response;
  }

  /**
   * Return user data to using on post request event.
   *
   * @return array<mixed>
   *   The user defined data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Set user data to using on post request event.
   *
   * @param array<mixed> $data
   *   The user defined data.
   *
   * @return \Drupal\hubspot_client\Event\SyncPostRequestEvent
   *   This object.
   */
  public function setData(array $data): self {
    $this->data = $data;
    return $this;
  }

}
