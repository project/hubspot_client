<?php

namespace Drupal\Tests\hubspot_client\tests\Kernel;

use Drupal\hubspot_client\Event\EntityMappingEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncFromEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncToEvent;
use Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent;
use Drupal\hubspot_client\Event\SyncFromEntityUpdateEvent;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;
use HubSpot\Crm\ObjectType;

/**
 * Test hubspot client events.
 *
 * @group hubspot_client
 */
class EventsTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'hubspot_client',
    'hubspot_api',
    'config',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * Test callback for EntityMappingEvent.
   */
  public function testEntityMappingEvent(): void {
    $event = new EntityMappingEvent([]);
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch($event);
    $data = $event->getEntityMapping();
    $this->assertArrayHasKey('user', $data);
    $this->assertEquals($data['user'], ObjectType::CONTACTS);
  }

  /**
   * Test callback for FieldMappingSyncFromEvent.
   */
  public function testFieldMappingSyncFromEvent(): void {
    $event = new FieldMappingSyncFromEvent();
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch($event);
    $data = $event->getFieldMapping();
    $this->assertIsArray($data);
    $this->assertArrayHasKey(ObjectType::CONTACTS, $data);
    $this->assertArrayHasKey('id', $data[ObjectType::CONTACTS]);
    $this->assertArrayHasKey('email', $data[ObjectType::CONTACTS]);
    $this->assertArrayHasKey('hs_language', $data[ObjectType::CONTACTS]);
    $this->assertArrayHasKey('hs_analytics_last_timestamp', $data[ObjectType::CONTACTS]);
  }

  /**
   * Test callback for FieldMappingSyncToEvent.
   */
  public function testFieldMappingSyncToEvent(): void {
    $user = User::create([
      'mail' => 'test@test.com',
    ]);
    $event = new FieldMappingSyncToEvent($user);
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch($event);
    $data = $event->getFieldMapping();
    $this->assertEquals($data['user']['properties']['email'], 'test@test.com');
    $this->assertEquals($data['user']['properties']['hs_language'], $user->getPreferredLangcode());
    $this->assertEquals($data['user']['properties']['lifecyclestage'], 'salesqualifiedlead');
  }

  /**
   * Test callback for EntityMappingEvent.
   */
  public function testEntitySyncIdentifyFieldsEvent(): void {
    $event = new FieldSyncIdentifyFieldsEvent();
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch($event);
    $data = $event->getFieldMapping();
    $this->assertArrayHasKey('user', $data);
    $this->assertArrayHasKey('email', $data['user']);
    $this->assertEquals($data['user']['email'], 'mail');
  }

  /**
   * Test callback for FieldMappingSyncToEvent.
   */
  public function testSyncFromEntityUpdateEvent(): void {
    $user = User::create([
      'mail' => 'test@test.com',
    ]);
    $event = new SyncFromEntityUpdateEvent([
      'entity_type' => ['user'],
      'entity' => $user,
    ]);
    $dispatcher = \Drupal::service('event_dispatcher');
    $dispatcher->dispatch($event);
    $type = $event->getHubspotEntityType();
    /** @var \Drupal\user\UserInterface $entity */
    $entity = $event->getHubspotEntity();
    $this->assertEquals($entity->getEmail(), 'test@test.com');
    $this->assertEquals($type, ['user']);
  }

}
