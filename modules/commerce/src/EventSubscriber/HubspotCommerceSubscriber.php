<?php

namespace Drupal\hubspot_commerce\EventSubscriber;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\hubspot_client\Event\EntityMappingEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncFromEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncToEvent;
use Drupal\hubspot_client\Event\SyncPreRequestEvent;
use Drupal\hubspot_client\SyncServiceInterface;
use Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent;
use Drupal\user\UserInterface;
use HubSpot\Client\Crm\Objects\Model\SimplePublicObjectWithAssociations;
use HubSpot\Crm\ObjectType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Hubspot commerce event subscriber.
 */
class HubspotCommerceSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The sync service.
   *
   * @var \Drupal\hubspot_client\SyncServiceInterface
   */
  protected $syncService;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChanel;

  /**
   * The Drupal entity to sync.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * {@inheritDoc}
   */
  public function __construct(SyncServiceInterface $sync_service, LoggerChannelInterface $logger_chanel) {
    $this->syncService = $sync_service;
    $this->loggerChanel = $logger_chanel;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      EntityMappingEvent::class => ['onEntityMapping'],
      FieldMappingSyncFromEvent::class => ['onFieldMappingFrom'],
      FieldMappingSyncToEvent::class => ['onFieldMappingTo'],
      FieldSyncIdentifyFieldsEvent::class => ['onIdentifyFieldMapping'],
      SyncPreRequestEvent::class => ['onPreRequest'],
    ];
  }

  /**
   * Returns the Hubspot object type for the given entity type.
   *
   * @return array<mixed>
   *   An array of entity types and their Hubspot object type.
   */
  public function commerceMap(): array {
    return [
      'commerce_order' => ObjectType::DEALS,
      'commerce_order_item' => ObjectType::LINE_ITEMS,
      'commerce_product_variation' => ObjectType::PRODUCTS,
    ];
  }

  /**
   * Kernel request event handler.
   *
   * @param \Drupal\hubspot_client\Event\EntityMappingEvent $event
   *   The event.
   */
  public function onEntityMapping(Event $event): void {
    $types = $event->getEntityMapping();
    $types = array_merge($types, $this->commerceMap());

    $event->setEntityMapping($types);
  }

  /**
   * Kernel request event handler.
   *
   * @param \Drupal\hubspot_client\Event\FieldMappingSyncFromEvent $event
   *   The event.
   */
  public function onFieldMappingFrom(Event $event): void {
    /** @var \HubSpot\Client\Crm\Objects\Model\SimplePublicObject $object */
    $object = $event->getObject();
    $field_mapping = $event->getFieldMapping();
    $this->dealsFieldMappingFrom($object, $field_mapping);
    $this->productFieldMappingFrom($object, $field_mapping);

    $event->setFieldMapping($field_mapping);
  }

  /**
   * Kernel request event handler.
   *
   * @param \Drupal\hubspot_client\Event\FieldMappingSyncToEvent $event
   *   The event.
   */
  public function onFieldMappingTo(Event $event): void {
    $this->entity = $event->getEntity();
    $field_mapping = $event->getFieldMapping();
    switch ($this->entity->getEntityTypeId()) {
      case 'commerce_order':
        $this->orderFieldMapping($field_mapping);
        break;

      case 'commerce_order_item':
        $this->orderItemFieldMapping($field_mapping);
        break;

      case 'commerce_product_variation':
        $this->productFieldMapping($field_mapping);
        break;
    }

    $event->setFieldMapping($field_mapping);
  }

  /**
   * Kernel request event handler.
   *
   * @param \Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent $event
   *   The event.
   */
  public function onIdentifyFieldMapping(Event $event): void {
    $field_mapping = $event->getFieldMapping();
    $field_mapping['commerce_order'] = [
      'hs_object_id' => 'order_number',
    ];
    $field_mapping['commerce_product_variation'] = [
      'hs_sku' => 'sku',
    ];

    $event->setFieldMapping($field_mapping);
  }

  /**
   * Prepare field mapping for commerce order item.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping.
   */
  protected function productFieldMapping(array &$field_mapping): void {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product */
    $product = $this->entity;
    $field_mapping[$product->getEntityTypeId()]['properties'] = [
      'name' => $product->label(),
      'hs_sku' => $product->getSku(),
    ];
  }

  /**
   * Prepare field mapping for commerce order.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping.
   */
  protected function orderFieldMapping(array &$field_mapping): void {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = clone $this->entity;
    $user = $order->getCustomer();
    if ($user instanceof UserInterface) {
      if ($user->getEmail() === NULL && $user->get('hubspot_id')->isEmpty()) {
        $this->loggerChanel->warning(
          'Requested email field is not exists for user @user. The order @nr not synced to hubspot.',
          [
            '@user' => $user->getDisplayName(),
            '@nr' => $order->getOrderNumber(),
          ],
        );
        return;
      }
      if ($user->get('hubspot_id')->isEmpty()) {
        if ($this->syncService->syncTo($user, 'create', $order) === FALSE) {
          $this->loggerChanel->warning(
            'User @user is not synced to hubspot. The order @nr is not synced to hubspot.',
            [
              '@user' => $user->getDisplayName(),
              '@nr' => $order->getOrderNumber(),
            ]
          );
          return;
        }
      }
    }
    $this->entity = $order;
    $total = $order->getTotalPrice() ?? 0;
    if ($total instanceof Price) {
      $total = $total->getNumber();
    }
    $order_number = $order->getOrderNumber();
    $field_mapping[$order->getEntityTypeId()]['properties'] = [
      'dealstage' => $this->getHubspotStatus($order->getState()->getString()),
      'dealname' => $this->t('Online Order #@nr', ['@nr' => $order_number])
        ->render(),
      'hs_object_id' => $order_number,
      'amount' => $total,
    ];
    if ($user instanceof UserInterface) {
      if ($user->hasField('hubspot_id') &&
        !$user->get('hubspot_id')->isEmpty()) {
        // The contacts_to_deals association.
        $field_mapping[$order->getEntityTypeId()]['associations'][] = [
          'to' => [
            'id' => $user->get('hubspot_id')->getString(),
          ],
          'types' => [
            [
              'associationCategory' => 'HUBSPOT_DEFINED',
              'associationTypeId' => '3',
            ],
          ],
        ];
      }
    }
  }

  /**
   * Prepare field mapping for commerce order item.
   *
   * @param array<mixed> $field_mapping
   *   The field mapping.
   */
  protected function orderItemFieldMapping(array &$field_mapping): void {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $line_item */
    $line_item = clone $this->entity;
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product */
    $product = $line_item->getPurchasedEntity();
    $order = $line_item->getOrder();
    $sku = '';
    $hs_product_id = '';
    if ($product instanceof ProductVariationInterface) {
      $hs_product_id = $this->getHubspotId($product);
      if (empty($hs_product_id === NULL)) {
        $this->syncService->syncTo($product, 'create');
        $hs_product_id = $this->getHubspotId($product);
      }
      $sku = $product->getSku();
    }
    $this->entity = $line_item;

    $quantity = (int) $this->entity->getQuantity();
    $unit_price = (float) $this->entity->getTotalPrice()->getNumber();
    if ($quantity > 0) {
      $unit_price /= $quantity;
    }
    // Auto association to product by hs_product_id.
    $field_mapping[$line_item->getEntityTypeId()]['properties'] = [
      'hs_sku' => $sku,
      'quantity' => $quantity,
      'price' => $unit_price,
      'hs_product_id' => $hs_product_id,
    ];
    if ($order) {
      $hs_order_id = $this->getHubspotId($order);
      if ($hs_product_id !== NULL) {
        // The deals_to_line_items association.
        $field_mapping[$line_item->getEntityTypeId()]['associations'][] = [
          'to' => [
            'id' => $hs_order_id,
          ],
          'types' => [
            [
              'associationCategory' => 'HUBSPOT_DEFINED',
              'associationTypeId' => '20',
            ],
          ],
        ];
      }
    }
  }

  /**
   * Deals field mapping.
   *
   * @param mixed $object
   *   The hubspot object.
   * @param array<mixed> $field_mapping
   *   The field mapping.
   */
  protected function dealsFieldMappingFrom($object, array &$field_mapping): void {
    $properties = $object !== NULL ? $object->getProperties() : [];
    $amount = $properties['amount'] ?? NULL;
    $currency = $properties['deal_currency_code'] ?? NULL;
    $total = '';
    if ($amount !== NULL && $currency !== NULL) {
      $total = new Price($amount, $currency);
    }
    $field_mapping[ObjectType::DEALS] = [
      'id' => [
        'id' => 'hubspot_id',
        'status' => TRUE,
        'value' => $object !== NULL ? $object->getId() : '',
      ],
      'hs_object_id' => [
        'id' => 'order_number',
        'status' => TRUE,
        'value' => $properties['hs_object_id'] ?? NULL,
      ],
      'dealstage' => [
        'id' => 'state',
        'status' => TRUE,
        'value' => $properties['dealstage'] ?? NULL,
      ],
    ];
    if ($total !== NULL) {
      $field_mapping[ObjectType::DEALS]['amount'] = [
        'id' => 'total_price',
        'status' => TRUE,
        'value' => $total,
      ];
    }
  }

  /**
   * Product field mapping.
   *
   * @param mixed $object
   *   The hubspot object.
   * @param array<mixed> $field_mapping
   *   The field mapping.
   */
  protected function productFieldMappingFrom($object, array &$field_mapping): void {
    $properties = $object !== NULL ? $object->getProperties() : [];

    $field_mapping[ObjectType::PRODUCTS] = [
      'id' => [
        'id' => 'hubspot_id',
        'status' => TRUE,
        'value' => $object !== NULL ? $object->getId() : '',
      ],
      'hs_sku' => [
        'id' => 'sku',
        'status' => TRUE,
        'value' => $properties['hs_sku'] ?? NULL,
      ],
      'name' => [
        'id' => 'title',
        'status' => TRUE,
        'value' => $properties['name'] ?? NULL,
      ],
    ];
  }

  /**
   * Contact field mapping.
   *
   * @param \Drupal\hubspot_client\Event\SyncPreRequestEvent $event
   *   The sync pre request event.
   */
  public function onPreRequest(Event $event): void {
    $map = $this->commerceMap();
    $entity = $event->getEntity();
    if (!in_array($entity->getEntityTypeId(), array_keys($map))) {
      return;
    }
    $data = $event->getData();
    $payload = $event->getPayload();
    $input = new SimplePublicObjectWithAssociations();
    $properties = $payload['properties'] ?? [];
    $input->setProperties($properties);
    $associations = $payload['associations'] ?? [];
    $input->setAssociations($associations);
    $data['payload'] = $input;
    $event->setData($data);
    $event->setObjectType($map[$entity->getEntityTypeId()]);
  }

  /**
   * Maping commerce status to hubspot status.
   *
   * @param string $status
   *   The commerce status.
   *
   * @return string
   *   The hubspot status.
   */
  protected function getHubspotStatus(string $status): string {
    $map = [
      'draft' => 'checkout_pending',
      'validation' => 'checkout_completed',
      'fulfillment' => 'processed',
      'completed' => 'shipped',
      'canceled' => 'cancelled',
    ];

    return $map[$status] ?? 'processed';
  }

  /**
   * Get hubspot id.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return string|null
   *   The hubspot id.
   */
  protected function getHubspotId(EntityInterface $entity):? string {
    $hubspot_id = NULL;
    if ($entity->hasField('hubspot_id')) {
      $hubspot_id = $entity->get('hubspot_id')->getString();
    }
    return $hubspot_id;
  }

}
