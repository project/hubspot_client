<?php

namespace Drupal\hubspot_sync\Event;

use Drupal\Component\EventDispatcher\Event;
use HubSpot\Client\Crm\Objects\Model\ModelInterface;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Drupal fields will be synced to which HubSpot
 * fields.
 *
 * @package Drupal\hubspot_client\Event
 */
class FieldSyncAssotiationFields extends Event {

  /**
   * The object that is being synced from hubspot.
   *
   * @var \HubSpot\Client\Crm\Objects\Model\ModelInterface
   */
  protected $object;

  /**
   * An array with the Drupal and Hubspot fields and their sync values.
   *
   * The array key is the Drupal field name while the id is the hubspot field
   * name.
   *
   * @var array<mixed>
   */
  protected $fieldMapping;

  /**
   * An array of entity load by properties.
   *
   * @var array<mixed>
   */
  protected $properties;

  /**
   * Constructs the object.
   *
   * @param \HubSpot\Client\Crm\Objects\Model\ModelInterface $object
   *   The object that is being synced from hubspot.
   * @param array<mixed> $field_mapping
   *   An array of Drupal field names with the Hubspot mapping information.
   * @param array<mixed> $properties
   *   An array of entity load by properties.
   */
  public function __construct(ModelInterface $object, array $field_mapping = [], array $properties = []) {
    $this->object = $object;
    $this->fieldMapping = $field_mapping;
    $this->properties = $properties;
  }

  /**
   * Gets the entity load by properties.
   *
   * @return array<mixed>
   *   The entity load by properties.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Sets the field mapping.
   *
   * @param array<mixed> $properties
   *   The entity load by properties.
   *
   * @return \Drupal\hubspot_sync\Event\FieldSyncAssotiationFields
   *   This object.
   */
  public function setProperties(array $properties): self {
    $this->properties = $properties;
    return $this;
  }

}
