<?php

namespace Drupal\hubspot_sync\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that gets dispatched when an entity is about to be synced.
 *
 * Allows modules to define which Drupal fields will be synced to which HubSpot
 * fields.
 *
 * @package Drupal\hubspot_client\Event
 */
class EntityLoadByPropertiesEvent extends Event {

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * An array of entity load by properties.
   *
   * @var array<mixed>
   */
  protected $properties;

  /**
   * Constructs the object.
   *
   * @param string $entity_type
   *   The entity type.
   * @param array<mixed> $properties
   *   An array of entity load by properties.
   */
  public function __construct(string $entity_type, array $properties = []) {
    $this->entityType = $entity_type;
    $this->properties = $properties;
  }

  /**
   * Gets the entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * Sets the entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return \Drupal\hubspot_sync\Event\EntityLoadByPropertiesEvent
   *   This object.
   */
  public function setEntityType(string $entity_type): self {
    $this->entityType = $entity_type;
    return $this;
  }

  /**
   * Gets the entity load by properties.
   *
   * @return array<mixed>
   *   The entity load by properties.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Sets the field mapping.
   *
   * @param array<mixed> $properties
   *   The entity load by properties.
   *
   * @return \Drupal\hubspot_sync\Event\EntityLoadByPropertiesEvent
   *   This object.
   */
  public function setProperties(array $properties): self {
    $this->properties = $properties;
    return $this;
  }

}
