<?php

namespace Drupal\hubspot_sync;

use Drupal\Core\Batch\BatchBuilder;

/**
 * Helper service interface for Drupal Hubspot syncing.
 */
interface HelperInterface {

  /**
   * Syncs hubspot objects to Drupal entities.
   *
   * @param string $direction
   *   The direction of syncing.
   */
  public function sync(string $direction): void;

  /**
   * Syncs hubspot objects to Drupal entities.
   *
   * @param string $entity_type
   *   The entity type to sync.
   * @param string $object_type
   *   The hubspot object type to sync.
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder.
   *
   * @return bool
   *   Whether operation added true otherwise false.
   */
  public function syncEntityTypeFrom(string $entity_type, string $object_type, BatchBuilder $batch_builder): bool;

  /**
   * Batch operation to process sync from.
   *
   * @param mixed $args
   *   The item to process.
   * @param array<mixed> $context
   *   The batch context.
   */
  public static function processBatchItemFrom(mixed $args, array &$context): void;

  /**
   * Batch finish callback from.
   *
   * @param bool $success
   *   Whether the batch process completed successfully.
   * @param array<mixed> $results
   *   The batch results.
   * @param array<mixed> $operations
   *   The batch operations.
   */
  public static function finishBatchProcessFrom(bool $success, array $results, array $operations): void;

  /**
   * Syncs Drupal entities to hubspot objects.
   *
   * @param string $entity_type
   *   The entity type to sync.
   * @param string $object_type
   *   The hubspot object type to sync.
   * @param \Drupal\Core\Batch\BatchBuilder $batch_builder
   *   The batch builder.
   *
   * @return bool
   *   Whether operation added true otherwise false.
   */
  public function syncEntityTypeTo(string $entity_type, string $object_type, BatchBuilder $batch_builder): bool;

  /**
   * Batch operation to process sync to.
   *
   * @param mixed $args
   *   The item to process.
   * @param array<mixed> $context
   *   The batch context.
   */
  public static function processBatchItemTo($args, array &$context): void;

  /**
   * Batch finish callback to.
   *
   * @param bool $success
   *   Whether the batch process completed successfully.
   * @param array<mixed> $results
   *   The batch results.
   * @param array<mixed> $operations
   *   The batch operations.
   */
  public static function finishBatchProcessTo($success, $results, $operations): void;

}
