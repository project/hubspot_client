<?php

namespace Drupal\hubspot_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Hubspot Sync settings for this site.
 */
class SyncForm extends FormBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueDatabaseFactory
   */
  protected $state;

  /**
   * The sync helper service.
   *
   * @var \Drupal\hubspot_sync\Helper
   */
  protected $syncHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->state = $container->get('keyvalue.database');
    $instance->syncHelper = $container->get('hubspot_sync.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_sync_settings';
  }

  /**
   * The form build function.
   *
   * @param array<mixed> $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array<mixed>
   *   The form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $sync_from = $this->state->get('hubspot_sync')->get('synced_from', FALSE);
    $help_text = $sync_from === TRUE ?
      $this->t('You can sync your Drupal data to Hubspot.') :
      $this->t('You can sync your Hubspot data to Drupal.');
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $help_text,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['sync_from'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync Hubspot ids to Drupal'),
      '#button_type' => 'primary',
      '#name' => 'sync_from',
    ];
    $form['actions']['sync_to'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync Drupal data to Hubspot'),
      '#button_type' => 'primary',
      '#name' => 'sync_to',
      '#disabled' => !$sync_from,
    ];

    return $form;
  }

  /**
   * Submit handler for the sync form.
   *
   * @param array<mixed> $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getTriggeringElement()['#name'] == 'sync_from') {
      $this->syncHelper->sync('from');
      $this->state->get('hubspot_sync')->set('synced_from', TRUE);
    }
    elseif ($form_state->getTriggeringElement()['#name'] == 'sync_to') {
      $this->syncHelper->sync('to');
    }
  }

}
