<?php

namespace Drupal\hubspot_sync;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hubspot_api\ManagerInterface;
use Drupal\hubspot_client\Event\EntityMappingEvent;
use Drupal\hubspot_client\Event\FieldMappingSyncFromEvent;
use Drupal\hubspot_client\Event\FieldSyncIdentifyFieldsEvent;
use Drupal\hubspot_sync\Event\EntityLoadByPropertiesEvent;
use HubSpot\Client\Crm\Objects\Model\PublicObjectSearchRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Helper service for Drupal Hubspot syncing.
 */
class Helper implements HelperInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An event dispatcher instance.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Hubspot handler class.
   *
   * @var \HubSpot\Discovery\Discovery
   */
  protected $client;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The batch builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Constructs a Helper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\hubspot_api\ManagerInterface $manager
   *   The hubspot manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EventDispatcherInterface $event_dispatcher,
    ManagerInterface $manager,
    LoggerChannelInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->client = $manager->getHandler();
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function sync(string $direction = 'from'): void {
    try {
      foreach ($this->getEntityTypes() as $entity_type => $object_type) {
        $batch_builder = new BatchBuilder();
        $has_operation = ($direction === 'from') ?
          $this->syncEntityTypeFrom($entity_type, $object_type, $batch_builder) :
          $this->syncEntityTypeTo($entity_type, $object_type, $batch_builder);
        if ($has_operation === FALSE) {
          continue;
        }
        $batch_builder->setTitle(
          $this->t('Sync Hubspot objects to Drupal entities'));
        $batch_builder->setInitMessage('Starting batch process...');
        $batch_builder->setProgressMessage('Processed @current out of @total.');
        $batch_builder->setErrorMessage('An error occurred during the batch process.');
        $callback = ($direction === 'from') ?
          [self::class, 'finishBatchProcessFrom'] :
          [self::class, 'finishBatchProcessTo'];
        $batch_builder->setFinishCallback($callback);
        // Start the batch process.
        batch_set($batch_builder->toArray());
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function syncEntityTypeFrom(string $entity_type, string $object_type, BatchBuilder $batch_builder): bool {
    $event = new FieldMappingSyncFromEvent();
    $this->eventDispatcher->dispatch($event);
    $mapping = $event->getFieldMapping();
    if (!isset($mapping[$object_type])) {
      return FALSE;
    }

    $process = FALSE;
    try {
      /** @var \HubSpot\Client\Crm\Objects\Model\PublicObjectSearchRequest $request */
      $request = new PublicObjectSearchRequest([]);
      /** @var \HubSpot\Client\Crm\Objects\Model\CollectionResponseWithTotalSimplePublicObjectForwardPaging $result */
      $result = $this->client->crm()->objects()->searchApi()
        ->doSearch($object_type, $request);
      $total = $total_count = $result->getTotal();
      $after = 0;
      $limit = 100;
      while ($total_count > 0) {
        $result_item = [
          'entity_type' => $entity_type,
          'object_type' => $object_type,
          'mapping' => $mapping[$object_type] ?? [],
          'total' => $total,
          'limit' => $limit,
          'after' => $after,
        ];
        $batch_builder->addOperation([self::class, 'processBatchItemFrom'], [$result_item]);
        $process = TRUE;
        $after += $limit;
        $total_count -= $limit;
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return $process;
  }

  /**
   * {@inheritDoc}
   */
  public static function processBatchItemFrom($args, array &$context): void {
    // Define total on first call.
    if (!isset($context['sandbox']['total'])) {
      $context['results'] = $args['limit'];
      $context['sandbox']['total'] = $args['total'];
      $context['sandbox']['success'] = 0;
      $context['sandbox']['processed'] = 0;
      $context['sandbox']['count'] = 0;
      $context['sandbox']['after'] = $args['after'];
    }

    $entity_type = $args['entity_type'];
    // Get the identify fields mapping.
    $event = new FieldSyncIdentifyFieldsEvent([]);
    \Drupal::service('event_dispatcher')->dispatch($event);
    $fields = $event->getFieldMapping()[$entity_type] ?? NULL;

    if ($fields === NULL) {
      $context['results'] = [
        'type' => $entity_type,
        'object_type' => $args['object_type'],
        'success' => 0,
        'count' => 0,
      ];
      $context['finished'] = TRUE;
      return;
    }

    $object_type = $args['object_type'];
    $mapping = $args['mapping'];
    $after = $context['sandbox']['after'];

    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

    $count = 0;
    $limit = 10;
    $time = \Drupal::time()->getCurrentMicroTime();
    if (!isset($context['sandbox']['time'])) {
      $context['sandbox']['time'] = $time;
    }
    elseif ($time < $context['sandbox']['time'] + 1000000) {
      // Last request was less than 1 second ago, wait a second.
      sleep(1);
    }

    try {
      /** @var \HubSpot\Client\Crm\Objects\Model\PublicObjectSearchRequest $request */
      $request = new PublicObjectSearchRequest([
        'properties' => array_keys($mapping),
        'limit' => $limit,
        'after' => $after,
      ]);
      $results = \Drupal::service('hubspot_api.manager')->getHandler()->crm()
        ->objects()->searchApi()->doSearch($object_type, $request)
        ->getResults();
    }
    catch (\Exception $e) {
    }

    $context['sandbox']['after'] += $limit;

    /** @var \HubSpot\Client\Crm\Objects\Model\SimplePublicObject $object */
    foreach ($results as $key => $object) {
      // Increment processed at one.
      $context['sandbox']['processed']++;
      $count++;
      $properties = [];
      // Get the property values from the hubspot object to find drupal object.
      foreach ($fields as $hubspot_name => $drupal_name) {
        $properties[$drupal_name] = $object->getProperties()[$hubspot_name];
      }
      $event = new EntityLoadByPropertiesEvent($entity_type, $properties);
      \Drupal::service('event_dispatcher')->dispatch($event);
      $properties = array_filter($event->getProperties());
      if (!empty($properties) && $entities = $storage->loadByProperties($properties)) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        foreach ($entities as $entity) {
          if ($entity->hasField('hubspot_id')) {
            if ($entity->get('hubspot_id')->isEmpty()) {
              // Set the hubspot id to drupal entity.
              $entity->set('hubspot_id', $object->getId());
              $entity->save();
              $context['sandbox']['success']++;
            }
            else {
              $context['sandbox']['success']++;
            }
          }
        }
      }

      // Remove current result.
      unset($results[$key]);

      if ($count >= $limit) {
        break;
      }
    }

    // Setup message to notify how many remaining articles.
    $context['message'] = t(
      'Updating @type ... processed @processed of @total.',
      [
        '@total' => $context['results'],
        '@type' => $args['object_type'],
        '@processed' => $context['sandbox']['processed'],
        '@success' => $context['sandbox']['success'],
      ]
    );

    // Set current step as unfinished until there's not results.
    $context['finished'] = $context['sandbox']['processed'] >= $context['results'] ||
      $context['sandbox']['after'] >= $context['sandbox']['total'];

    // When it is completed, then setup result as total amount updated.
    if ($context['finished']) {
      $context['results'] = [
        'type' => $entity_type,
        'object_type' => $args['object_type'],
        'success' => $context['sandbox']['success'],
        'count' => $context['sandbox']['total'],
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function syncEntityTypeTo(string $entity_type, string $object_type, BatchBuilder $batch_builder): bool {
    $process = FALSE;
    // Set the items to process.
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $query = $storage->getQuery();
    if ($entity_type === 'commerce_order_item') {
      $query->condition('order_id', NULL, 'IS NOT NULL');
    }
    try {
      $entities = $query->execute();
      if (!empty($entities)) {
        $offset = 0;
        $callback = [self::class, 'processBatchItemTo'];
        while ($offset < count($entities)) {
          $slices = array_slice($entities, $offset, 100);
          $offset += 100;
          $results = [
            'entity_type' => $entity_type,
            'object_type' => $object_type,
            'total' => count($entities),
            'items' => $slices,
          ];
          $batch_builder->addOperation($callback, [$results]);
          $process = TRUE;
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return $process;
  }

  /**
   * {@inheritDoc}
   */
  public static function processBatchItemTo($args, array &$context): void {
    // Define total on first call.
    if (!isset($context['sandbox']['total'])) {
      $context['results'] = $args['items'];
      $context['sandbox']['total'] = $args['total'];
      $context['sandbox']['success'] = 0;
      $context['sandbox']['processed'] = 0;
    }
    $entity_type = $args['entity_type'];

    // Init limit variable.
    $limit = 10;
    // Walk-through all results in order to update them.
    $count = 0;
    $storage = \Drupal::service('entity_type.manager')
      ->getStorage($entity_type);

    foreach ($context['results'] as $key => $id) {
      $count++;
      $context['sandbox']['processed']++;
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $storage->load($id);
      if (!$entity instanceof EntityInterface) {
        // Remove current result.
        unset($context['results'][$key]);
        continue;
      }
      if ($entity_type === 'commerce_order_item') {
        $entity = $entity->set('hubspot_id', NULL);
      }
      $entity->get('hubspot_id')->isEmpty() ?
        \Drupal::service('hubspot_client.sync')->syncTo($entity, 'create') :
        \Drupal::service('hubspot_client.sync')->syncTo($entity, 'update');
      $context['sandbox']['success']++;
      // Prepare Hubspot line items.
      if ($entity_type === 'commerce_order') {
        if (!$entity->get('hubspot_id')->isEmpty()) {
          // Delete existing Hubspot line items of deal.
          self::deleteLineItemsOfDeal($entity->get('hubspot_id')->getString());
        }
      }

      // Remove current result.
      unset($context['results'][$key]);

      if ($count >= $limit) {
        break;
      }
    }

    // Setup message to notify how many remaining articles.
    $context['message'] = t(
      'Updating @type ... @total pending...',
      ['@total' => count($context['results']), '@type' => $args['entity_type']]
    );

    // Set current step as unfinished until there's not results.
    $context['finished'] = empty($context['results']);

    // When it is completed, then setup result as total amount updated.
    if ($context['finished']) {
      $context['results'] = [
        'type' => $entity_type,
        'object_type' => $args['object_type'],
        'success' => $context['sandbox']['success'],
        'count' => $context['sandbox']['total'],
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function finishBatchProcessFrom($success, $results, $operations): void {
    // Setup final message after process is done.
    $message = ($success) ?
      t('Updated @type from @count @object.',
        [
          '@success' => $results['success'],
          '@count' => $results['count'],
          '@type' => $results['type'],
          '@object' => $results['object_type'],
        ]) :
      t('Finished with an error.');
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * {@inheritDoc}
   */
  public static function finishBatchProcessTo($success, $results, $operations): void {
    // Setup final message after process is done.
    $message = ($success) ?
      t('Updated @count @type to Hubspot @object.',
        [
          '@success' => $results['success'],
          '@count' => $results['count'],
          '@type' => $results['type'],
          '@object' => $results['object_type'],
        ]) :
      t('Finished with an error.');
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Get the entity types to sync.
   *
   * @return array<mixed>
   *   The entity types keyed by drupal entity type.
   */
  protected function getEntityTypes(): array {
    $event = new EntityMappingEvent([]);
    $this->eventDispatcher->dispatch($event);
    $entity_types = $event->getEntityMapping();

    return $entity_types;
  }

  /**
   * Get the entity types to sync.
   *
   * @param string $hubspot_id
   *   The object hubspot id.
   */
  protected static function deleteLineItemsOfDeal(string $hubspot_id): void {
    /** @var \HubSpot\Discovery\Discovery $client */
    $client = \Drupal::service('hubspot_api.manager')->getHandler();
    try {
      $line_items = $client->crm()->deals()->associationsApi()
        ->getAll((int) $hubspot_id, 'line_item')->getResults();
      /** @var \HubSpot\Client\Crm\Deals\Model\MultiAssociatedObjectWithLabel $line_item */
      foreach ($line_items as $line_item) {
        $client->crm()->lineItems()->basicApi()
          ->archive((string) $line_item->getToObjectId());
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('hubspot_api')->error($e->getMessage());
    }
  }

}
