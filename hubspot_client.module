<?php

/**
 * @file
 * Primary module hooks for Hubspot Client module.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function hubspot_client_help(string $route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.hubspot_client':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t(
          'The Hubspot Client module integrates Drupal with Hubspot.
        It syncs data with the <a target="_blank" href="@hubspot_api">Hubspot API</a>
        any time a user (order, or product) is created or updated.', [
          '@hubspot_api' => 'https://github.com/HubSpot/hubspot-api-php',
        ]
      ) . '</p>';

      return $output;
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function hubspot_client_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() === 'user') {
    $fields['hubspot_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hubspot ID'))
      ->setDescription(t('The Hubspot ID for entity.'))
      ->setSettings([
        'max_length' => 255,
        'default_value' => NULL,
      ]);
  }

  return $fields;
}

/**
 * Implements hook_entity_insert().
 */
function hubspot_client_entity_insert(EntityInterface $entity) {
  if ($entity instanceof ContentEntityInterface &&
    $entity->hasField('hubspot_id')) {
    \Drupal::service('hubspot_client.sync')->syncTo($entity, 'create');
  }
}

/**
 * Implements hook_entity_update().
 */
function hubspot_client_entity_update(EntityInterface $entity) {
  if ($entity instanceof ContentEntityInterface &&
    $entity->hasField('hubspot_id')) {
    \Drupal::service('hubspot_client.sync')->syncTo($entity, 'update');
  }
}

/**
 * Implements hook_entity_delete().
 */
function hubspot_client_entity_delete(EntityInterface $entity) {
  if ($entity instanceof ContentEntityInterface &&
    $entity->hasField('hubspot_id')) {
    \Drupal::service('hubspot_client.sync')->syncTo($entity, 'archive');
  }
}
